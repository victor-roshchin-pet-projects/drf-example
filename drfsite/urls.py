"""drfsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework import routers, mixins
from women.views import *
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

# Для автоматизации генерации маршрутов API используются роутеры.
# Роутеры упрощают написания маршрутов для VmewSets.
# Не нужно под каждое представление прописывать urlpatterns,
# достаточно создать экземпляр роутера и зарагистрировать
# в нем класс представлений ViewSets.


# # Пользовательский роутер.
# class MyCustomRouter(routers.SimpleRouter):
#
#     # Список из маршрутов.
#     routes = [
#         # Каждый элемент является объектом класса Route и определяет один отдельный маршрут.
#         # url - шаблон маршрута.
#         # В примере маршруты указаны без последнего обратного слэша.
#         # mapping связывает тип запроса с методом обработки.
#         # name - название маршрута.
#         # detail - список или отдельная запись.
#         # initkwargs - дополнительные аргументы, которые даются
#         # конкретному определению при срабатываении маршрута.
#         routers.Route(url=r'^{prefix}$',
#                       mapping={'get': 'list'},
#                       name='{basename}-list',
#                       detail=False,
#                       initkwargs={'suffix': 'List'}),
#         routers.Route(url=r'^{prefix}/{lookup}$',
#                       mapping={'get': 'retrieve'},
#                       name='{basename}-detail',
#                       detail=True,
#                       initkwargs={'suffix': 'Detail'})
#     ]

# # Создаем экземпляр пользовательского роутера.
# router = MyCustomRouter()
#
# # Создаем экземпляр роутера.
# router = routers.SimpleRouter()


# # У DefaultRouter присутствуется маршрут api/v1/,
# # у SimpleRouter такой маршрут отсутствует.
# router = routers.DefaultRouter()

# # Регистрируем WomenViewSet в роутера и роутер автоматически генерирует коллекцию urls.
# # Префикс для набора маршрутов - r'women', связанное представление WomenViewSet.
# # Имя маршрута принимается по связанной модели.
# # Можно переназначить через параметр 'basename=<newname>'.
# # Если queryset в представлении не указано, то параметр basename обязателен.
# router.register(r'women', WomenViewSet, basename='women')

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     path('api/v1/', include(router.urls)),
#     # path('api/v1/womenlist/', WomenViewSet.as_view({'get': 'list'})),
#     # path('api/v1/womenlist/<int:pk>/', WomenViewSet.as_view({'put': 'update'})),
# ]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/women/', WomenAPIList.as_view()),
    path('api/v1/women/<int:pk>/', WomenAPIUpdate.as_view()),
    path('api/v1/womendelete/<int:pk>/', WomenAPIDestroy.as_view()),
    path('api/v1/drf-auth/', include('rest_framework.urls')),
    # path('api/v1/drf-auth/', include('rest_framework.urls')),
    path('api/v1/auth/', include('djoser.urls')),          # new
    re_path(r'^auth/', include('djoser.urls.authtoken')),  # new

    # Эти классы представлений поставляются вместе с пакетом djangorestframework-simplejwt.
    path('api/v1/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]
