from django.forms import model_to_dict
from django.shortcuts import render
from rest_framework import generics, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
from .permissions import *
from .serializers import WomenSerializer




# # Общий класс (объединяет все виды запросов).
# class WomenViewSet(viewsets.ModelViewSet):
#     # queryset = Women.objects.all()
#     serializer_class = WomenSerializer
#
#     # Переопределение для возвращения данных из БД
#     # (для более гибкой настроки, добавления сортировки, фильтров и тд).
#     def get_queryset(self):
#
#         # Проверяем, есть ли в маршруте параметр pk (запрос определенной статьи).
#         pk = self.kwargs.get('pk')
#
#         # Если нет, то возвращаем список всех статей.
#         if not pk:
#             return Women.objects.all()
#
#         # Если есть, возвращаем эту статью.
#         # get_queryset должен возвращать список в любом случае,
#         # поэтому применяется filter.
#         return Women.objects.filter(pk=pk)
#
#     # Для создания новых маршрутов используются декоратор action.
#     # Например, если мы хотим вывести список категорий через это же представление.
#     # Список категорий хранится в другой (связанной) модели (таблицы БД).
#     # methods - разрешенные методы, detail - возвращать только одну запись.
#     # Если detail=True необходимо передать pk и взять запись через get.
#     # Новый маршрут формируется по имене метода.
#     @action(methods=['get'], detail=False)
#     def category(self, request):
#         cats = Category.objects.all()
#         return Response({'cats': [{'id': c.id, 'name': c.name} for c in cats]})


# Класс представления пагинации.
class WomenAPIListPagination(PageNumberPagination):

    # Количество записей по умолчанию.
    page_size = 3

    # Дополнительный параметр, который можно указать в GET запросе,
    # чтобы изменить page_size по умолчанию.
    page_size_query_param = 'page_size'

    # Максимальное количество записей на странице.
    max_page_size = 10000


# Класс для GET и POST запроса (возвращает записи и добавляет запись).
class WomenAPIList(generics.ListCreateAPIView):

    # Возвращаемые данные.
    queryset = Women.objects.all()

    # Применяемый сериализатор.
    # Сериализатор определяется в отдельном файле serializers.
    serializer_class = WomenSerializer

    # Атрибут отвечает за выбранные классы ограничений. Принимает кортеж.
    # Указывает, что читать записи могут все, а у авторизованного пользователя
    # появляется дополнительно возможность добавить запись.
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # Класс для пагинации.
    pagination_class = WomenAPIListPagination


# Класс для PUT и PATCH запросов (меняет определенную запись).
class WomenAPIUpdate(generics.RetrieveUpdateAPIView):

    # В данном случае выполняется ленивый запрос.
    # На самом деле здесь отбирается только одлна запись, а не все.
    queryset = Women.objects.all()

    serializer_class = WomenSerializer

    permission_classes = (IsAuthenticated,)

    # Параметр указывает какой способ аутификации будет использоваться.
    # authentication_classes = (TokenAuthentication, )


# Класс для GET и DELETE запросов определенной одной записи.
class WomenAPIDestroy(generics.RetrieveDestroyAPIView):
    queryset = Women.objects.all()
    serializer_class = WomenSerializer

    # Атрибут разрешает удаление только для админов сайта.
    permission_classes = (IsAdminOrReadOnly,)





# # Класс APIView представляет собой самый базовый класс для всех классов API.
# class WomenAPIView(APIView):
#
#     # Метод, отвечающий за обработку get запросов.
#     def get(self, request):
#
#         # Получаем список объектов из модели (БД).
#         w = Women.objects.all()
#
#         # Класс Response формирует JSON строку из словаря python.
#         # Передаем список w на серилизатор, он обрабатывает данные (кодирует) и выдает обратно.
#         # many=True - Сериализатор должен обрабатывать не одну запись, а список записей.
#         # Затем Response преобразует данные в байтовую JSON строку.
#         return Response({'get': WomenSerializer(w, many=True).data})
#
#     # Метод, отвечающий за обработку post запросов.
#     def post(self, request):
#
#         # При получении неправильных данных, необходимо сделать проверку.
#         # Создадим сеарилизатор из полученных данных.
#         serializator = WomenSerializer(data=request.data)
#
#         # Проверяем корректность принятых данных. В случае ошибок генерируем исключение
#         # и клиент получает ответ в виде JSON строки.
#         serializator.is_valid(raise_exception=True)
#
#         # Этот метод автоматически вызовет метод create у WomenSerializer и запись сохранится в БД.
#         serializator.save()
#
#         # # Переменная, которая буде ссылаться на новую запись в модели Women.
#         # # Объект автоматически сохранится.
#         # # Атрибуты будут браться из коллекции request.data по ключам.
#         # post_new = Women.objects.create(
#         #     title=request.data['title'],
#         #     content=request.data['content'],
#         #     cat_id=request.data['cat_id']
#         # )
#
#         # # Функция model_to_dict преобразует объект (экземпляр) модели в словарь.
#         # return Response({'post': model_to_dict(post_new)})
#
#         # Передаем объект на серилизатор, он обрабатывает данные (кодирует) и выдает обратно.
#         return Response({'post': serializator.data})
#
#     # Метод
#     def put(self, request, *args, **kwargs):
#
#         # Определяем идентификатор записи, которую нужно поменять.
#         pk = kwargs.get('pk', None)
#
#         # Проверяем, указан ли идентификатор изменяемой записи в запросе.
#         if not pk:
#
#             # Если ключ в запросе отсутствует - генерируется ошибка.
#             return Response({"error": "PUT key is not defined"})
#
#         # Если ключ присутствует.
#         try:
#             # Пытаемся взять запись по ключу из модели.
#             instance = Women.objects.get(pk=pk)
#         except:
#
#             # Если записи нет, генерируется исключение и отлавливаем его, возвращая ошибку.
#             return Response({"error": "Object does not exist"})
#
#         # Если все прошло успешно создаем сеарилизатор, проверяем данные и сохраняем.
#         serializer = WomenSerializer(data=request.data, instance=instance)
#         serializer.is_valid(raise_exception=True)
#
#         # В данном случае метод save вызовет метод update, а не create.
#         serializer.save()
#
#         # Возвращаем измененные данные.
#         return Response({"put": serializer.data})
#
#
#     def delete(self, request, *args, **kwargs):
#         pk = kwargs.get('pk', None)
#         if not pk:
#             return Response({"error": "DELETE key is not defined"})
#         try:
#             instance = Women.objects.get(pk=pk)
#             instance.delete()
#         except:
#             return Response({"error": "Object does not exist"})
#
#         return Response({"delete": "Object deleted by id = " + str(pk)})
