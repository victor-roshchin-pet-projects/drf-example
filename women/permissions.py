from rest_framework import permissions


# Класс для определения права доступа на удаление записи (только админу).
class IsAdminOrReadOnly(permissions.BasePermission):

    # Переопределяем функция has_permission. В данном случае к конкретной записи не обращаемся.
    def has_permission(self, request, view):

        # Проверяем, является ли метод запроса безопасным.
        # Безопасные методы - 'GET', 'HEAD', 'OPTIONS'
        if request.method in permissions.SAFE_METHODS:

            # Предоставляем право доступа.
            return True

        # Иначе даем доступ только авторизованным пользователям,
        # являющимся администраторами.
        return bool(request.user and request.user.is_staff)


# Класс для определения права доступа на редактирование записи (только автору).
class IsOwnerOrReadOnly(permissions.BasePermission):

    # В данном случае необходимо сравнить атрибут конкретной записи (obj.user)
    # с параметром запроса (request.user).
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.user == request.user