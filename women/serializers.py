import io

from rest_framework import serializers
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

from .models import Women


# Класс работает с моделями (БД) напрямую.
class WomenSerializer(serializers.ModelSerializer):

    # Дополнительный атрибут для идентификации пользователи.
    # Создается скрытое поле и по умолчанию в нем прописывается текущий пользователь.
    # При добавлении записи, этот параметр автоматически определится и передастся в БД.
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:

        # Связываем сеарилизатор с моделью.
        model = Women

        # # Прописываем поля, которые будем возвращать клиенту.
        # # В данном случае работаем с моделью, поэтому ключ cat указываем без _id.
        # fields = ('id', 'title', 'content', 'cat')

        # __all__ - для отображения всех полей.
        fields = '__all__'



# # ЭКВИВАЛЕНТНО КЛАССУ ВЫШЕ WomenSerializer(serializers.ModelSerializer)

# # Класс описания модели сериализатора.
# class WomenSerializer(serializers.Serializer):
#
#     # title является строкой, для этого используется serializers.CharField().
#     title = serializers.CharField(max_length=255)
#
#     # content также является строкой, но будет без ограничения.
#     content = serializers.CharField()
#
#     time_create = serializers.DateTimeField(read_only=True)
#
#     time_update = serializers.DateTimeField(read_only=True)
#
#     is_published = serializers.BooleanField(default=True)
#
#     # Ключ связи таблиц в данном случае прописывается как целочисленное значение,
#     # потому в JSON строке будет фигурировать параметр cat_id с целым числом.
#     cat_id = serializers.IntegerField()
#
#     # Метод для создания записи (для POST запроса).
#     # Словарь validated_data состоит из всех проверенных данных (после метода is_valed).
#     def create(self, validated_data):
#         return Women.objects.create(**validated_data)
#
#     # Метод для изменения существующей записи в БД.
#     # instance - ссылка на модель (объект Women).
#     def update(self, instance, validated_data):
#
#         # Изменеяем соответствующий локальный атрубут при помощи метода get
#         # по ключу (название атрибута. Если значения по ключу не найдется, значит get
#         # вернет прежний параметр.
#         instance.title = validated_data.get('title', instance.title)
#         instance.content = validated_data.get('content', instance.content)
#         instance.time_update = validated_data.get('time_update', instance.time_update)
#         instance.is_published = validated_data.get('is_published', instance.is_published)
#         instance.cat_id = validated_data.get('cat_id', instance.cat_id)
#
#         # Сохраняем изменения в модели (БД).
#         instance.save()
#
#         return instance







# ОБЩАЯ СХЕМА РАБОТЫ СЕРИАЛИЗАЦИИ (ДЛЯ ПРИМЕРА).

# # Гипотетический класс модели.
# # class WomenModel:
# #     def __init__(self, title, content):
# #         self.title = title
# #         self.content = content
#
#
# # Класс сериализатора наследуется от WomenModel.
# # Атрибуты класса сериализатора имеют названия абсолютно такие же,
# # как и в обрабатываемой модели.
# class WomenSerializer(serializers.Serializer):
#
#     # title является строкой, для этого используется serializers.CharField().
#     title = serializers.CharField(max_length=255)
#
#     # content также является строкой, но будет без ограничения.
#     content = serializers.CharField()
#
#
# # Кодирования данных в JSON.
# def encode():
#
#     # Создаем экземпляр WomenModel.
#     model = WomenModel('Angelina Jolie', 'Content: Angelina Jolie')
#
#     # Объект после сериализации.
#     # Когда создается объект сериализации, отрабатывает мета-класс,
#     # который создает специальную коллекцию data из атрибутов передаваемого объекта.
#     model_sr = WomenSerializer(model)
#
#     print(model_sr.data, type(model_sr.data), sep='\n')
#
#     # JSONRenderer (render) преобразует объект сериализации в байтовую JSON строку.
#     json = JSONRenderer().render(model_sr.data)
#
#     print(json)
#
#
# # Декодирования данных из JSON.
# def decode():
#
#     # Имметируется поступления запроса от клиента. Создается байтовая строка JSON.
#     stream = io.BytesIO(b'{"title": "Angelina Jolie", "content": " Content: Angelina Jolie"}')
#
#     # JSONParser (parse) преобразует байтовую строку JSON в JSON объект. объект (словарь) python.
#     data = JSONParser().parse(stream)
#
#     # Используя сериализатор WomenSerializer преобразовываем data в объект сериализации.
#     # Когда объект декодируется необходимо передавать аргумент с именованным параметром data.
#     serializer = WomenSerializer(data=data)
#
#     # Проверяем данные в сериализаторе на корректность.
#     serializer.is_valid()
#
#     # После проверки в сериализаторе появится коллекция (словарь) validated_data.
#     print(serializer.validated_data)


